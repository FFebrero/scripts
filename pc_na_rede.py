import nmap

def scan_network():
    # Crea un objeto de escaneo Nmap
    nm = nmap.PortScanner()

    # Realiza el escaneo en tu red local
    nm.scan(hosts='192.168.1.0/24', arguments='-sn')

    # Itera sobre los resultados del escaneo y muestra los dispositivos encontrados
    for host in nm.all_hosts():
        if 'mac' in nm[host]['addresses']:
            mac_addr = nm[host]['addresses']['mac']
        else:
            mac_addr = "No disponible"
        print(f"Dispositivo encontrado: {nm[host]['vendor']} ({host}) [{mac_addr}]")

if __name__ == "__main__":
    scan_network()
